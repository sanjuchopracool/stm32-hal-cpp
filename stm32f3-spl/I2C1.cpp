#include "I2C1.h"

#include <stm32f30x_i2c.h>

I2C1Driver::I2C1Driver()
{
    // enable I2C1
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
    //GPIO alternate function
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_4);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_4);

    // enable pins
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* I2C_InitStuct*/
    I2C_InitTypeDef  I2C_InitStruct;
    I2C_InitStruct.I2C_Timing = 0x10C08DCF; //0xC062121F;  0x10C091CF;
    I2C_InitStruct.I2C_AnalogFilter = I2C_AnalogFilter_Enable; // +
    I2C_InitStruct.I2C_DigitalFilter = 0x00; // +
    I2C_InitStruct.I2C_Mode = I2C_Mode_I2C; // +
    I2C_InitStruct.I2C_OwnAddress1 = 0x00; // +
    I2C_InitStruct.I2C_Ack = I2C_Ack_Enable; // +
    I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;

}

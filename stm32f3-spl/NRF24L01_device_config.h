#ifndef NRF24L01_CONFIG_H
#define NRF24L01_CONFIG_H

#include <system.h>

// These macros should be specific to microcontroller

extern void EnableSlave();
extern void DisableSlave();
// Enable NRF24L01P as slave
#define EnableNRF24L01P()   EnableSlave()
// Disable NRF24L01P as slave
#define DisableNRF24L01P()  DisableSlave()

// Enable transfer for NRF24L01P
#define EnableTransferNRF24L01P()
// Disable transfer for  NRF24L01P
#define DisableTransferNRF24L01P()

// Function to delay in microseconds
#define NRF2401LPDelayUs( x ) delayMicroseconds(x)
#endif // NRF24L01_CONFIG_H

/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "system.h"
#include <stdbool.h>
#include <serial.h>
#include <NRF24L01P.h>
#include <L3GD20Gyro.h>
#include <math.h>

#include "PWM.h"
#include "PID.h"
#include "VoltageReader.h"
#include "stm32f3_discovery_lsm303dlhc.h"

extern "C" {
#include <FreeRTOS.h>
#include <task.h>
//#include "queue.h"
}

// MACROS
#define TRANSFER_SIZE 10
#define PI (float) 3.14159265f
#define LSM_Acc_Sensitivity_2g     (float)     1.0f            /*!< accelerometer sensitivity with 2 g full scale [LSB/mg] */
#define LSM_Acc_Sensitivity_4g     (float)     0.5f            /*!< accelerometer sensitivity with 4 g full scale [LSB/mg] */
#define LSM_Acc_Sensitivity_8g     (float)     0.25f           /*!< accelerometer sensitivity with 8 g full scale [LSB/mg] */
#define LSM_Acc_Sensitivity_16g    (float)     0.0834f         /*!< accelerometer sensitivity with 12 g full scale [LSB/mg] */

///////////////////////////////////////////////////////////////////////////////

// variables
GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
uint8_t data[TRANSFER_SIZE];
NRF24L01P* theRfPtr;
L3GD20Gyro* theGyro;
VoltageReader* theBattery;
PID* thePid;

uint8_t pwm = 0;

double throttle = 0;
double pitch;
double roll;
///////////////////////////////////////////////////////////////////////////////

float fNormAcc,fSinRoll,fCosRoll,fSinPitch,fCosPitch = 0.0f, AccRollAng = 0.0f, AccPitchAng = 0.0f;
//xQueueHandle xQueue;

void toggleLED(void* data)
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;

        if( ++pwm > 5)
            pwm= 5;
        vTaskDelay( 250 );
    }
    vTaskDelete(NULL);
}

void error()
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;
        delay( 100 );
    }
}

float AccBuffer[3] = {0.0f};
void Demo_CompassReadAcc(float* pfData)
{
  int16_t pnRawData[3];
  uint8_t ctrlx[2];
  uint8_t buffer[6], cDivider;
  uint8_t i = 0;
  float LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_2g;

  /* Read the register content */
  LSM303DLHC_Read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG4_A, ctrlx,2);
  LSM303DLHC_Read(ACC_I2C_ADDRESS, LSM303DLHC_OUT_X_L_A, buffer, 6);

  if(ctrlx[1]&0x40)
    cDivider=64;
  else
    cDivider=16;

  /* check in the control register4 the data alignment*/
  if(!(ctrlx[0] & 0x40) || (ctrlx[1] & 0x40)) /* Little Endian Mode or FIFO mode */
  {
    for(i=0; i<3; i++)
    {
      pnRawData[i]=((int16_t)((uint16_t)buffer[2*i+1] << 8) + buffer[2*i])/cDivider;
    }
  }
  else /* Big Endian Mode */
  {
    for(i=0; i<3; i++)
      pnRawData[i]=((int16_t)((uint16_t)buffer[2*i] << 8) + buffer[2*i+1])/cDivider;
  }
  /* Read the register content */
  LSM303DLHC_Read(ACC_I2C_ADDRESS, LSM303DLHC_CTRL_REG4_A, ctrlx,2);


  if(ctrlx[1]&0x40)
  {
    /* FIFO mode */
    LSM_Acc_Sensitivity = 0.25;
  }
  else
  {
    /* normal mode */
    /* switch the sensitivity value set in the CRTL4*/
    switch(ctrlx[0] & 0x30)
    {
    case LSM303DLHC_FULLSCALE_2G:
      LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_2g;
      break;
    case LSM303DLHC_FULLSCALE_4G:
      LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_4g;
      break;
    case LSM303DLHC_FULLSCALE_8G:
      LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_8g;
      break;
    case LSM303DLHC_FULLSCALE_16G:
      LSM_Acc_Sensitivity = LSM_Acc_Sensitivity_16g;
      break;
    }
  }

  /* Obtain the mg value for the three axis */
  for(i=0; i<3; i++)
  {
    pfData[i]=(float)pnRawData[i]/LSM_Acc_Sensitivity;
  }

}
void loop(void* p)
{
//    portTickType xlastwakeTime = xTaskGetTickCount();
    while(1)
    {
//        if ( theRfPtr->Readable())
//        {
//            theRfPtr->read(NRF24L01P::RX_PIPE::PIPE_0, data, TRANSFER_SIZE);
//            if( 'A' == data[0] && 'A' == data[1] && 'A' == data[2] && 'Z' == data[9] )
//            {
//                throttle = (((int32_t)((data[3] << 8) | data[4])*100) >> 16);
//                pitch = (((int32_t)((data[7] << 8) | data[8])*100) >> 16);
//                roll = (((int32_t)((data[5] << 8) | data[6])*100) >> 16);
//                printf((char*)"%d, %d, %d\n", (int16_t)throttle, (int16_t)pitch, (int16_t)roll);
//            }
//        }
        volatile double gyroRate[3] = {0};
        theGyro->readAngularRate(gyroRate);
        printf((char*)"%d, %d, %d\n", (int16_t)gyroRate[0], (int16_t)gyroRate[1], (int16_t)gyroRate[2]);
        vTaskDelay( 100 );
    }
}

#define alpha	(float) 0.5
void readAcc( void * p)
{
    while(1 )
    {
//        Demo_CompassReadAcc(AccBuffer);
//        for(uint8_t i=0;i<3;i++)
//            AccBuffer[i] = AccBuffer[i] * alpha + (AccBuffer[i] * (1.0 - alpha));
//        AccRollAng = atan2f(AccBuffer[1], AccBuffer[2])*180.0/PI;
//        AccPitchAng = -atan2f(AccBuffer[0], sqrt(AccBuffer[1]*AccBuffer[1] + AccBuffer[2]*AccBuffer[2]))*180.0/PI;
//        printf((char*)"pitch %d, roll %d\n", (int16_t)AccPitchAng, (int16_t)AccRollAng);
        vTaskDelay( 500 );
    }
}
int main(void)
{
    // it will start sysTick

    systemInit();

    initSerial();

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );

    /* Configure PE14 and PE15 in output pushpull mode */
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    printf( (char*)"Printf supported !\n" );
//    initPwm(); // keep motor at zero

//    NRF24L01P theRf( SPIInterface::SPI_2 );
//    theRfPtr = &theRf;
//    theRf.SetAirDataRate(NRF24L01P::DataRate::DR_250_KBPS);
//    theRf.SetCrcWidth(NRF24L01P::CRCWidth::CRC_16_BIT);
//    theRf.SetRfFrequency( 2402 );
//    theRf.SetTransferSize( TRANSFER_SIZE );
//    theRf.PowerUp();
//    theRf.SetReceiveMode();
//    theRf.SetEnable( true );

//    VoltageReader battery;
//    theBattery = &battery;

    L3GD20Gyro gyro( SPIInterface::SPI_1 );
    printf((char*)"GyroID: %d", gyro.GetDeviceID());
    theGyro = &gyro;

//    // setup accelerometer
//    LSM303DLHCAcc_InitTypeDef LSM303DLHCAcc_InitStructure;
//    LSM303DLHCAcc_FilterConfigTypeDef LSM303DLHCFilter_InitStructure;

//    /* Fill the accelerometer structure */
//    LSM303DLHCAcc_InitStructure.Power_Mode = LSM303DLHC_NORMAL_MODE;
//    LSM303DLHCAcc_InitStructure.AccOutput_DataRate = LSM303DLHC_ODR_50_HZ;
//    LSM303DLHCAcc_InitStructure.Axes_Enable= LSM303DLHC_AXES_ENABLE;
//    LSM303DLHCAcc_InitStructure.AccFull_Scale = LSM303DLHC_FULLSCALE_2G;
//    LSM303DLHCAcc_InitStructure.BlockData_Update = LSM303DLHC_BlockUpdate_Continous;
//    LSM303DLHCAcc_InitStructure.Endianness=LSM303DLHC_BLE_LSB;
//    LSM303DLHCAcc_InitStructure.High_Resolution=LSM303DLHC_HR_ENABLE;
//    /* Configure the accelerometer main parameters */
//    LSM303DLHC_AccInit(&LSM303DLHCAcc_InitStructure);

//    /* Fill the accelerometer LPF structure */
//    LSM303DLHCFilter_InitStructure.HighPassFilter_Mode_Selection =LSM303DLHC_HPM_NORMAL_MODE;
//    LSM303DLHCFilter_InitStructure.HighPassFilter_CutOff_Frequency = LSM303DLHC_HPFCF_16;
//    LSM303DLHCFilter_InitStructure.HighPassFilter_AOI1 = LSM303DLHC_HPF_AOI1_DISABLE;
//    LSM303DLHCFilter_InitStructure.HighPassFilter_AOI2 = LSM303DLHC_HPF_AOI2_DISABLE;

//    /* Configure the accelerometer LPF main parameters */
//    LSM303DLHC_AccFilterConfig(&LSM303DLHCFilter_InitStructure);

    xTaskCreate( loop, (const signed char*)"VTask", 1024, NULL,1,NULL);
    vTaskStartScheduler();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif

extern NRF24L01P theRf;
void EXTIRQHandler()
{
    if( GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_10) == RESET )
    {
        EXTI_ClearITPendingBit(EXTI_Line10);
//        theRfPtr->read(NRF24L01P::RX_PIPE::PIPE_0, data, TRANSFER_SIZE);
//        if( 'A' == data[0] && 'A' == data[1] && 'A' == data[2] && 'Z' == data[9] )
//        {
//            throttle = (((int32_t)((data[3] << 8) | data[4])*100) >> 16);
//            pitch = (((int32_t)((data[7] << 8) | data[8])*100) >> 16);
//            roll = (((int32_t)((data[5] << 8) | data[6])*100) >> 16);
//            printf((char*)"%d, %d, %d\n", (int16_t)throttle, (int16_t)pitch, (int16_t)roll);
//        }
    }
}

uint32_t LSM303DLHC_TIMEOUT_UserCallback ()
{
    while ( 1 )
    {
        printf((char*)"I2c timeout\n");
    }
    return 0;
}

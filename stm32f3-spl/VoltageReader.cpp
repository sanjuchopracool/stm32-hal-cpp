#include "VoltageReader.h"

#include "stm32f30x_adc.h"

#include <system.h>

__IO uint32_t calibration_value = 0;

VoltageReader::VoltageReader()
{
    /* Configure the ADC clock */
    RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
    /* Enable ADC1 clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
    /* ADC Channel configuration */
    /* GPIOC Periph clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    init();
}

VoltageReader::~VoltageReader()
{

}

void VoltageReader::init()
{
    ADC_InitTypeDef       ADC_InitStructure;
    ADC_CommonInitTypeDef ADC_CommonInitStructure;
    GPIO_InitTypeDef    GPIO_InitStructure;

    /* Configure ADC Channel7 as analog input */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 ;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    ADC_StructInit(&ADC_InitStructure);

    /* Calibration procedure */
    ADC_VoltageRegulatorCmd(ADC1, ENABLE);
    delayMicroseconds(10);
    ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
    ADC_StartCalibration(ADC1);

    while(ADC_GetCalibrationStatus(ADC1) != RESET );
    calibration_value = ADC_GetCalibrationValue(ADC1);

    ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
    ADC_CommonInitStructure.ADC_Clock = ADC_Clock_AsynClkMode;
    ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    ADC_CommonInitStructure.ADC_DMAMode = ADC_DMAMode_OneShot;
    ADC_CommonInitStructure.ADC_TwoSamplingDelay = 0;

    ADC_CommonInit(ADC1, &ADC_CommonInitStructure);

    ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
    ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
    ADC_InitStructure.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
    ADC_InitStructure.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Disable;
    ADC_InitStructure.ADC_AutoInjMode = ADC_AutoInjec_Disable;
    ADC_InitStructure.ADC_NbrOfRegChannel = 1;
    ADC_Init(ADC1, &ADC_InitStructure);

    /* ADC1 regular channel1 configuration */
    ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 1, ADC_SampleTime_7Cycles5);

    /* Enable ADC1 */
    ADC_Cmd(ADC1, ENABLE);

    /* wait for ADRDY */
    while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));

    /* Start ADC1 Software Conversion */
    ADC_StartConversion(ADC1);
}

void VoltageReader::deInit()
{

}

void VoltageReader::readVoltage(double& outBatteryVoltage)
{
    while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
    outBatteryVoltage = (double)ADC_GetConversionValue(ADC1)*11.76/0xFFF + 0.6;
}

#include "PID.h"

#include "stdint.h"
#include "PWM.h"
#include "printf.h"

static double previousPitchError;
static double previousRollError;
static double previousYawError;

static double pitchError;
static double rollError;
static double yawError;

static double kPPitch = 0;
static double kIPitch = 0;
static double kDPitch = 40;

static double kPRoll = kPPitch;
static double kIRoll = kIPitch;
static double kDRoll = kDPitch;

static double kPYaw = 0.0;
static double kIYaw = 0.0;
static double kDYaw = 0;

static double pitchInput;
static double rollInput;
static double yawInput;

static double pitchIntegral;
static double rollIntegral;
static double yawIntegral;

static double pitchOutput;
static double rollOutput;
static double yawOutput;

static double pwm1;
static double pwm2;
static double pwm3;
static double pwm4;

inline void setAbsoluteMax( double& outvalue, const double& inValue)
{
    if( outvalue > inValue)
        outvalue = inValue;
    else if( outvalue < (-1.0*inValue) )
        outvalue = -1.0*inValue;
}

inline void setAbsolutePositiveMax( double& outvalue, const double& inValue)
{
    if( outvalue > inValue)
        outvalue = inValue;
    else if ( outvalue < 0.0 )
        outvalue = 0.0;
}

PID::PID()
{
}

PID::~PID()
{

}

void PID::update(const double& inThrottle,
                 const double &inPitch,
                 const double &inRoll,
                 const double& inVoltage,
                 const volatile double* gyroRate,
                 bool stop)
{
    double inTempRoll = inThrottle;
    if ( inTempRoll > 90.0 )
        inTempRoll = 90;

    pitchInput = (pitchInput*0.5) + gyroRate[0]*0.5;
    rollInput = (rollInput*0.5) + gyroRate[1]*0.5;
    yawInput = (yawInput*0.5) + gyroRate[2]*0.5;


    // find error TODO add setpoint
    pitchError = pitchInput ;//- inPitch;
    rollError = rollInput ;//- inRoll;
    yawError = yawInput;

    // integral part
    pitchIntegral += pitchError*kIPitch;
    rollIntegral += rollError*kIRoll;
    yawIntegral += yawError*kIYaw;

    // limit integral part to max of 10 for pitch and roll
    setAbsoluteMax(pitchIntegral, 5.0);
    setAbsoluteMax(rollIntegral, 5.0);

    // calculate output
    pitchOutput = kPPitch*pitchError + pitchIntegral + kDPitch*( pitchError - previousPitchError );
    rollOutput = kPRoll*rollError + rollIntegral + kDRoll*( rollError - previousRollError );
    yawOutput = kPYaw*yawError + yawIntegral + kDYaw*( yawError - previousYawError );

    yawOutput = 0;
    //update previous errors
    previousPitchError = pitchError;
    previousRollError = rollError;
    previousYawError = yawError;

//    esc_4 = throttle - pid_output_pitch - pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 4 (front-left - CW)
//    esc_1 = throttle - pid_output_pitch + pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 1 (front-right - CCW)
//    esc_2 = throttle + pid_output_pitch + pid_output_roll + pid_output_yaw; //Calculate the pulse for esc 2 (rear-right - CW)
//    esc_3 = throttle + pid_output_pitch - pid_output_roll - pid_output_yaw; //Calculate the pulse for esc 3 (rear-left - CCW)

    // pwm output
    pwm1 = inThrottle - pitchOutput - rollOutput + yawOutput; //front left(CW), it will rotate CCW
    pwm2 = inThrottle - pitchOutput + rollOutput - yawOutput; //front right(CCW), it will rotate CW
    pwm3 = inThrottle + pitchOutput + rollOutput + yawOutput; //rear right(CW)
    pwm4 = inThrottle + pitchOutput - rollOutput - yawOutput; // rear left(CCW)

    if( stop )
    {
        setPwm(CHANNEL1, 0);
        setPwm(CHANNEL2, 0);
        setPwm(CHANNEL3, 0);
        setPwm(CHANNEL4, 0);
    }
    else
    {
        //handle the voltage drop
        if( inVoltage > 9 && inVoltage < 11.20 )
        {
            pwm1 *= (1.0 + ( 11.20 - inVoltage)/18 );
            pwm2 *= (1.0 + ( 11.20 - inVoltage)/18 );
            pwm3 *= (1.0 + ( 11.20 - inVoltage)/18 );
            pwm4 *= (1.0 + ( 11.20 - inVoltage)/18 );
        }

        static double maxthrottle = 40;
        setAbsolutePositiveMax(pwm1, maxthrottle);
        setAbsolutePositiveMax(pwm2, maxthrottle);
        setAbsolutePositiveMax(pwm3, maxthrottle);
        setAbsolutePositiveMax(pwm4, maxthrottle);

        setPwm(CHANNEL1, pwm1 );
        setPwm(CHANNEL2, pwm2 );
        setPwm(CHANNEL3, pwm3 );
        setPwm(CHANNEL4, pwm4 );
    }
//    printf((char*)"%d, %d, %d, %d  stoped %d\n" , (int16_t)pwm1, (int16_t)pwm2, (int16_t)pwm3, (int16_t)pwm4, stop);
}

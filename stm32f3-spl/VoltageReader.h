#ifndef VOLTAGEREADER_H
#define VOLTAGEREADER_H

#include <stdint.h>

class VoltageReader
{
public:
    VoltageReader();
    ~VoltageReader();

    void init();
    void deInit();

    void readVoltage(double& outBatteryVoltage);
};

#endif // VOLTAGEREADER_H

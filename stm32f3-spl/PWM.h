#ifndef PWM_H
#define PWM_H

enum PWM_CHANNEL
{
    CHANNEL1,
    CHANNEL2,
    CHANNEL3,
    CHANNEL4
};

void initPwm();
//duty should be 30.13 format
void setPwm( PWM_CHANNEL inChannel, float dutyCycle );
#endif // PWM_H

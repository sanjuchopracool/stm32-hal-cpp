/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "system.h"
#include <stdbool.h>
#include <serial.h>
#include <NRF24L01P.h>
#include <L3GD20Gyro.h>
#include "PWM.h"
#include "PID.h"
#include "VoltageReader.h"

extern "C" {
#include <FreeRTOS.h>
#include <task.h>
//#include "queue.h"
}

// MACROS
#define TRANSFER_SIZE 10
///////////////////////////////////////////////////////////////////////////////

// variables
GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
uint8_t data[TRANSFER_SIZE];
NRF24L01P* theRfPtr;
uint8_t pwm = 0;

double throttle = 0;
double pitch;
double roll;
///////////////////////////////////////////////////////////////////////////////

//xQueueHandle xQueue;

void toggleLED(void* data)
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;

        if( ++pwm > 5)
            pwm= 5;
        vTaskDelay( 250 );
    }
    vTaskDelete(NULL);
}

void error()
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;
        delay( 100 );
    }
}



void loop(void* p)
{
//    portTickType xlastwakeTime = xTaskGetTickCount();
    while(1)
    {
//        if ( theRfPtr->Readable())
//        {
//            theRfPtr->read(NRF24L01P::RX_PIPE::PIPE_0, data, TRANSFER_SIZE);
//            if( 'A' == data[0] && 'A' == data[1] && 'A' == data[2] && 'Z' == data[9] )
//            {
//                throttle = (((int32_t)((data[3] << 8) | data[4])*100) >> 16);
//                pitch = (((int32_t)((data[7] << 8) | data[8])*100) >> 16);
//                roll = (((int32_t)((data[5] << 8) | data[6])*100) >> 16);
//                printf((char*)"%d, %d, %d\n", (int16_t)throttle, (int16_t)pitch, (int16_t)roll);
//            }
//        }
    }
}
int main(void)
{
    // it will start sysTick

    systemInit();

    initSerial();

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );

    /* Configure PE14 and PE15 in output pushpull mode */
    GPIO_InitStructure.GPIO_Pin = (GPIO_Pin_15 |GPIO_Pin_14 |GPIO_Pin_13 |GPIO_Pin_12 |GPIO_Pin_11 | GPIO_Pin_10 | GPIO_Pin_9 | GPIO_Pin_8);
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    printf( (char*)"Printf supported !\n" );
    initPwm(); // keep motor at zero

    NRF24L01P theRf( SPIInterface::SPI_2 );
    theRfPtr = &theRf;
    theRf.SetAirDataRate(NRF24L01P::DataRate::DR_250_KBPS);
    theRf.SetCrcWidth(NRF24L01P::CRCWidth::CRC_16_BIT);
    theRf.SetRfFrequency( 2402 );
    theRf.SetTransferSize( TRANSFER_SIZE );
    theRf.PowerUp();
    theRf.SetReceiveMode();
    theRf.SetEnable( true );

    xTaskCreate( loop, (const signed char*)"VTask", 1024, NULL,1,NULL);
    vTaskStartScheduler();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif

extern NRF24L01P theRf;
void EXTIRQHandler()
{
    if( GPIO_ReadOutputDataBit(GPIOB, GPIO_Pin_10) == RESET )
    {
        EXTI_ClearITPendingBit(EXTI_Line10);
        theRfPtr->read(NRF24L01P::RX_PIPE::PIPE_0, data, TRANSFER_SIZE);
        if( 'A' == data[0] && 'A' == data[1] && 'A' == data[2] && 'Z' == data[9] )
        {
            throttle = (((int32_t)((data[3] << 8) | data[4])*100) >> 16);
            pitch = (((int32_t)((data[7] << 8) | data[8])*100) >> 16);
            roll = (((int32_t)((data[5] << 8) | data[6])*100) >> 16);
            printf((char*)"%d, %d, %d\n", (int16_t)throttle, (int16_t)pitch, (int16_t)roll);
        }
    }
}

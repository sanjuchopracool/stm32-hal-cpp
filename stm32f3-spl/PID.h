#ifndef PID_H
#define PID_H


class PID
{
public:
    PID();
    ~PID();

    void update(const double &inThrottle, const double &inPitch, const double &inRoll ,const  double &inBattery, const volatile double* gyroRate, bool stop );
};

#endif // PID_H

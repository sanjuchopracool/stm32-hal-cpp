/**
  ******************************************************************************
  * @file    SysTick/SysTick_Example/main.c
  * @author  MCD Application Team
  * @version V1.1.2
  * @date    14-August-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "system.h"
#include <stdbool.h>
#include <serial.h>

extern "C" {
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

}

// MACROS
#define TRANSFER_SIZE 10
///////////////////////////////////////////////////////////////////////////////

// variables
GPIO_InitTypeDef        GPIO_InitStructure;
static bool isSet = false;
xSemaphoreHandle xMutex = NULL;
///////////////////////////////////////////////////////////////////////////////

void toggleLED(void* data)
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;

        vTaskDelay( 250 );
    }
    vTaskDelete(NULL);
}

void error()
{
    while(1)
    {
        if(isSet)
            GPIO_ResetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        else
            GPIO_SetBits( GPIOE, GPIO_InitStructure.GPIO_Pin );
        isSet = !isSet;
        delay( 100 );
    }
}



void loop( void* pData )
{
    char* taskName = (char*)pData;
    while(1)
    {
        printf( "%s before yield\n", taskName );
        taskYIELD();
        if( NULL != xSemaphoreTake( xMutex, 10 ))
        {
            printf( "%s After yield\n", taskName );
            xSemaphoreGive( xMutex );
        }
    }
}

static const char* task1Name = "Task1";
static const char* task2Name = "Task2";

int main(void)
{
    // it will start sysTick

    systemInit();

    initSerial();

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOE, ENABLE );

    xMutex = xSemaphoreCreateMutex();
    printf( (char*)"Printf supported !\n" );

    xTaskCreate( loop, (const signed char*)task1Name, 128, (void*)task1Name, 1, NULL);
    xTaskCreate( loop, (const signed char*)task2Name, 128, (void*)task2Name, 1, NULL);
    vTaskStartScheduler();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    error();
}
#endif

void EXTIRQHandler()
{
    EXTI_ClearITPendingBit(EXTI_Line10);
}

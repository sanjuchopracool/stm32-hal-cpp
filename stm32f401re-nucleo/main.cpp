/**
  ******************************************************************************
  * @file    Project/STM32F4xx_StdPeriph_Templates/main.c
  * @author  MCD Application Team
  * @version V1.6.1
  * @date    21-October-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2015 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <system.h>
#include <serial.h>
#include <NRF24L01P.h>

/** @addtogroup Template_Project
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */

int main(void)
{
    systemInit();

    initSerial();
    tfp_printf((char*)"Hello World\n");

    printf( (char*)"Printf supported !\n" );

    NRF24L01P theRf( SPIInterface::SPI_1 );
    theRf.SetAirDataRate(NRF24L01P::DataRate::DR_250_KBPS);
    int theStatus = (NRF24L01P::DataRate::DR_250_KBPS == theRf.GetAirDataRate() );
    tfp_printf( (char*)"Data rate 250: %d\n", theStatus );
    theRf.SetRfFrequency( 2402 );
    tfp_printf( (char*)"2402: %d\n", theRf.GetRfFrequency() );
    theStatus = ( NRF24L01P::TX_PWR::ZERO_DB == theRf.GetRfOutputPower() );
    tfp_printf( (char*)"ZERO_DB: %d\n", theStatus );
    unsigned long long address = theRf.GetTxAddress();
    unsigned long lower = (address & 0xFFFFFFFF);
    unsigned long upper = (address >> 32);
    printf( (char*)"TX Address  %08X%08X\n", upper, lower);
    address = theRf.GetRxAddress();
    lower = (address & 0xFFFFFFFF);
    upper = (address >> 32);
    printf( (char*)"TX Address  %08X%08X\n", upper, lower);

    /* Infinite loop */
    theRf.PowerUp();
    theRf.SetTransferSize( 32 );
    printf( (char*)"Transfer Size: %d\n", theRf.GetTransferSize());
    theRf.SetTransmitMode();
    theRf.SetEnable( true );

    char txData[33];
    for ( int i = 0; i < 32; i++ )
        txData[i] = 'A' + i;

    txData[32] = 0;
    while (1)
    {
        printf((char*) "sending %s", txData);
        theRf.write( NRF24L01P::RX_PIPE::PIPE_0, txData, 32 );
        delay(500);
    }
}


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

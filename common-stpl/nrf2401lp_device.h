#ifndef NRF24L01PDEVICE_H
#define NRF24L01PDEVICE_H

#include "NRF24L01_device_config.h"
#include "WriteReadInterface.h"

class NRF24L01PDevice
{
public:
    uint8_t getRegister( uint8_t inReg );
    void setRegister( uint8_t inReg, uint8_t inRegData );
    void setWriteReadInterface( WriteReadInterface* inInterface );

private:
    WriteReadInterface* mWriteRead;
};

#endif // NRF24L01PDEVICE_H

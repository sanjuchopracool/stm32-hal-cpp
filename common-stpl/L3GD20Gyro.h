#ifndef L3GD20GYRO_H
#define L3GD20GYRO_H

#include <SPI.h>

class L3GD20Gyro
{
public:
    L3GD20Gyro( SPIInterface::SPIName inName );
    ~L3GD20Gyro();

    void Init();
    void Deinit();
    uint8_t GetDeviceID();
    /**
     * @brief readAngularRate
     * @param outValues (in Degree per second)
     */
    void readAngularRate(volatile double *outValues);

private:
    void readRawRate(int16_t *outValues);
    void ReadRegister(uint8_t* outBuffer, uint8_t inReadAddr, uint16_t inNumByteToRead);
    void WriteRegister(uint8_t* inBuffer, uint8_t inWriteAddr, uint16_t inNumByteToWrite);

private:
    SPIInterface mSPI;
};

#endif // L3GD20GYRO_H

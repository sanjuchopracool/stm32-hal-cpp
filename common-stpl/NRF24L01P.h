#ifndef NRF24L01P_H
#define NRF24L01P_H

#include "SPI.h"

#define NRF24L01P_MIN_RF_FREQUENCY    2400
#define NRF24L01P_MAX_RF_FREQUENCY    2525

#define DEFAULT_NRF24L01P_ADDRESS       ((unsigned long long) 0xE7E7E7E7E7 )
#define DEFAULT_NRF24L01P_ADDRESS_WIDTH  5
#define DEFAULT_NRF24L01P_RF_FREQUENCY  (NRF24L01P_MIN_RF_FREQUENCY + 2)
#define DEFAULT_NRF24L01P_TRANSFER_SIZE  4

class NRF24L01P
{
public:

    enum class TX_PWR : uint8_t
    {
        ZERO_DB = 0,
        MINUS_6_DB,
        MINUS_12_DB,
        MINUS_18_DB,
        TX_PWR_INVALID
    };

    enum class DataRate : uint8_t
    {
        DR_250_KBPS = 0,
        DR_1_MBPS,
        DR_2_MBPS,
        DR_INVALID
    };

    enum class CRCWidth : uint8_t
    {
        CRC_NONE = 0,
        CRC_8_BIT,
        CRC_16_BIT,
        CRC_INVALID
    };

    enum class RX_PIPE : uint8_t
    {
        PIPE_0 = 0,
        PIPE_1,
        PIPE_2,
        PIPE_3,
        PIPE_4,
        PIPE_5
    };

    enum class ChipMode : uint8_t
    {
        UNKNOWN,
        POWER_DOWN,
        STANDBY,
        RX,
        TX
    };

    NRF24L01P( SPIInterface::SPIName inName );
    ~NRF24L01P();

    void SetRfFrequency( uint16_t inFrequency = DEFAULT_NRF24L01P_RF_FREQUENCY);
    uint16_t GetRfFrequency ();

    void SetRfOutputPower( TX_PWR inPwr = TX_PWR::ZERO_DB );
    TX_PWR GetRfOutputPower();

    void SetAirDataRate( DataRate inDataRate = DataRate::DR_250_KBPS );
    DataRate GetAirDataRate();

    void SetCrcWidth( CRCWidth inCRCWidth = CRCWidth::CRC_NONE );
    CRCWidth GetCrcWidth();

    void SetRxAddress(unsigned long long address = DEFAULT_NRF24L01P_ADDRESS,
                      RX_PIPE inPipe = RX_PIPE::PIPE_0 );

    void SetRxAddress(unsigned long msb_address,
                      unsigned long lsb_address,
                      RX_PIPE inPipe = RX_PIPE::PIPE_0 );

    void SetTxAddress(unsigned long long address = DEFAULT_NRF24L01P_ADDRESS);

    void SetTxAddress(unsigned long msb_address,
                      unsigned long lsb_address);

    unsigned long long GetRxAddress( RX_PIPE inPipe = RX_PIPE::PIPE_0 );
    unsigned long long GetTxAddress();

    void SetTransferSize( uint8_t inSize = DEFAULT_NRF24L01P_TRANSFER_SIZE,
                          RX_PIPE inPipe = RX_PIPE::PIPE_0);

    int GetTransferSize(RX_PIPE inPipe = RX_PIPE::PIPE_0);

    void SetReceiveMode();
    void SetTransmitMode();

    void PowerUp();
    void PowerDown();

    void SetEnable( bool inEnable );

    int write(RX_PIPE pipe, char *data, int count);
    int read(RX_PIPE pipe, uint8_t *data, int count);

    bool Readable(RX_PIPE inPipe = RX_PIPE::PIPE_0);

    void DisableAllRxPipes(void);

    void DisableAutoAcknowledge();
    void enableAutoAcknowledge( RX_PIPE inPipe = RX_PIPE::PIPE_0 );

    void DisableAutoRetransmit();

    void Init();
    void Deinit();

    uint8_t GetRegister( uint8_t inReg );
private:
    void SetRegister( uint8_t inReg, uint8_t inRegData );
    int GetStatusRegister();

private:
    SPIInterface mSPI;
    bool mEnabled{ false };
    ChipMode mMode;
};

#endif // NRF24L01P_H

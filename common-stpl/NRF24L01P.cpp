#include "NRF24L01P.h"

#include <system.h>

#ifdef STM32F3DISCOVERY
#include <stm32f30x.h>
#endif

#ifdef NUCLEO401RE
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#endif

//////////////////////////////////////////////////////////////////////////////


/*
 * The following FIFOs are present in nRF24L01+:
 *   TX three level, 32 byte FIFO
 *   RX three level, 32 byte FIFO
 */
#define _NRF24L01P_TX_FIFO_COUNT   3
#define _NRF24L01P_RX_FIFO_COUNT   3

#define _NRF24L01P_TX_FIFO_SIZE   32
#define _NRF24L01P_RX_FIFO_SIZE   32

#define _NRF24L01P_SPI_MAX_DATA_RATE     10000000

#define _NRF24L01P_SPI_CMD_RD_REG            0x00
#define _NRF24L01P_SPI_CMD_WR_REG            0x20
#define _NRF24L01P_SPI_CMD_RD_RX_PAYLOAD     0x61
#define _NRF24L01P_SPI_CMD_WR_TX_PAYLOAD     0xa0
#define _NRF24L01P_SPI_CMD_FLUSH_TX          0xe1
#define _NRF24L01P_SPI_CMD_FLUSH_RX          0xe2
#define _NRF24L01P_SPI_CMD_REUSE_TX_PL       0xe3
#define _NRF24L01P_SPI_CMD_R_RX_PL_WID       0x60
#define _NRF24L01P_SPI_CMD_W_ACK_PAYLOAD     0xa8
#define _NRF24L01P_SPI_CMD_W_TX_PYLD_NO_ACK  0xb0
#define _NRF24L01P_SPI_CMD_NOP               0xff


#define _NRF24L01P_REG_CONFIG                0x00
#define _NRF24L01P_REG_EN_AA                 0x01
#define _NRF24L01P_REG_EN_RXADDR             0x02
#define _NRF24L01P_REG_SETUP_AW              0x03
#define _NRF24L01P_REG_SETUP_RETR            0x04
#define _NRF24L01P_REG_RF_CH                 0x05
#define _NRF24L01P_REG_RF_SETUP              0x06
#define _NRF24L01P_REG_STATUS                0x07
#define _NRF24L01P_REG_OBSERVE_TX            0x08
#define _NRF24L01P_REG_RPD                   0x09
#define _NRF24L01P_REG_RX_ADDR_P0            0x0a
#define _NRF24L01P_REG_RX_ADDR_P1            0x0b
#define _NRF24L01P_REG_RX_ADDR_P2            0x0c
#define _NRF24L01P_REG_RX_ADDR_P3            0x0d
#define _NRF24L01P_REG_RX_ADDR_P4            0x0e
#define _NRF24L01P_REG_RX_ADDR_P5            0x0f
#define _NRF24L01P_REG_TX_ADDR               0x10
#define _NRF24L01P_REG_RX_PW_P0              0x11
#define _NRF24L01P_REG_RX_PW_P1              0x12
#define _NRF24L01P_REG_RX_PW_P2              0x13
#define _NRF24L01P_REG_RX_PW_P3              0x14
#define _NRF24L01P_REG_RX_PW_P4              0x15
#define _NRF24L01P_REG_RX_PW_P5              0x16
#define _NRF24L01P_REG_FIFO_STATUS           0x17
#define _NRF24L01P_REG_DYNPD                 0x1c
#define _NRF24L01P_REG_FEATURE               0x1d

#define _NRF24L01P_REG_ADDRESS_MASK          0x1f

// CONFIG register:
#define _NRF24L01P_CONFIG_PRIM_RX        (1<<0)
#define _NRF24L01P_CONFIG_PWR_UP         (1<<1)
#define _NRF24L01P_CONFIG_CRC0           (1<<2)
#define _NRF24L01P_CONFIG_EN_CRC         (1<<3)
#define _NRF24L01P_CONFIG_MASK_MAX_RT    (1<<4)
#define _NRF24L01P_CONFIG_MASK_TX_DS     (1<<5)
#define _NRF24L01P_CONFIG_MASK_RX_DR     (1<<6)

#define _NRF24L01P_CONFIG_CRC_MASK       (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)
#define _NRF24L01P_CONFIG_CRC_NONE       (0)
#define _NRF24L01P_CONFIG_CRC_8BIT       (_NRF24L01P_CONFIG_EN_CRC)
#define _NRF24L01P_CONFIG_CRC_16BIT      (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)

// EN_AA register:
#define _NRF24L01P_EN_AA_NONE            0

// EN_RXADDR register:
#define _NRF24L01P_EN_RXADDR_NONE        0

// SETUP_AW register:
#define _NRF24L01P_SETUP_AW_AW_MASK      (0x3<<0)
#define _NRF24L01P_SETUP_AW_AW_3BYTE     (0x1<<0)
#define _NRF24L01P_SETUP_AW_AW_4BYTE     (0x2<<0)
#define _NRF24L01P_SETUP_AW_AW_5BYTE     (0x3<<0)

// SETUP_RETR register:
#define _NRF24L01P_SETUP_RETR_NONE       0

// RF_SETUP register:
#define _NRF24L01P_RF_SETUP_RF_PWR_MASK          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_0DBM          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM    (0x2<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM   (0x1<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM   (0x0<<1)

#define _NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT       (1 << 3)
#define _NRF24L01P_RF_SETUP_RF_DR_LOW_BIT        (1 << 5)
#define _NRF24L01P_RF_SETUP_RF_DR_MASK           (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT|_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_250KBPS        (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_1MBPS          (0)
#define _NRF24L01P_RF_SETUP_RF_DR_2MBPS          (_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)

// STATUS register:
#define _NRF24L01P_STATUS_TX_FULL        (1<<0)
#define _NRF24L01P_STATUS_RX_P_NO        (0x7<<1)
#define _NRF24L01P_STATUS_MAX_RT         (1<<4)
#define _NRF24L01P_STATUS_TX_DS          (1<<5)
#define _NRF24L01P_STATUS_RX_DR          (1<<6)

// RX_PW_P0..RX_PW_P5 registers:
#define _NRF24L01P_RX_PW_Px_MASK         0x3F

#define _NRF24L01P_TIMING_Tundef2pd_us     100000   // 100mS
#define _NRF24L01P_TIMING_Tstby2a_us          130   // 130uS
#define _NRF24L01P_TIMING_Thce_us              10   //  10uS
#define _NRF24L01P_TIMING_Tpd2stby_us        4500   // 4.5mS worst case
#define _NRF24L01P_TIMING_Tpece2csn_us          4   //   4uS

//////////////////////////////////////////////////////////////////////////////


#ifdef STM32F3DISCOVERY
#define NRF24L01P_CS_PIN                        GPIO_Pin_12
#define NRF24L01P_CS_GPIO_PORT                  GPIOB

#define NRF24L01P_CE_PIN                        GPIO_Pin_11
#define NRF24L01P_CE_GPIO_PORT                  GPIOB

#define NRF24L01P_IRQ_PIN                        GPIO_Pin_10
#define NRF24L01P_IRQ_GPIO_PORT                  GPIOB
#define NRF24L01P_IRQ_PORT_SOURCE                EXTI_PortSourceGPIOB
#define NRF24L01P_IRQ_PIN_SOURCE                 EXTI_PinSource10
#define NRF24L01P_IRQ_EXT_LINE                   EXTI_Line10
#define NRF24L01P_CS_CLK_Enable()               RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB , ENABLE )
#define NRF24L01P_CE_CLK_Enable()               RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE )
#define NRF24L01P_IRQ_CLK_Enable()               RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE )
#endif

#ifdef NUCLEO401RE
#define NRF24L01P_CS_CLK_Enable()               RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOB, ENABLE )
#define NRF24L01P_CE_CLK_Enable()               RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOC, ENABLE )
#define NRF24L01P_IRQ_CLK_Enable()              RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOB, ENABLE )

#define NRF24L01P_CS_PIN                        GPIO_Pin_6
#define NRF24L01P_CS_GPIO_PORT                  GPIOB

#define NRF24L01P_CE_PIN                        GPIO_Pin_7
#define NRF24L01P_CE_GPIO_PORT                  GPIOC

#define NRF24L01P_IRQ_PIN                        GPIO_Pin_9
#define NRF24L01P_IRQ_GPIO_PORT                  GPIOA
#define NRF24L01P_IRQ_PORT_SOURCE                EXTI_PortSourceGPIOA
#define NRF24L01P_IRQ_PIN_SOURCE                 EXTI_PinSource9
#define NRF24L01P_IRQ_EXT_LINE                   EXTI_Line9
#endif

#define NRF24L01_CLEAR_INTERRUPTS()   TM_NRF24L01_WriteRegister(0x07, 0x70)
namespace
{

void EnableSlave()
{
    GPIO_ResetBits( NRF24L01P_CS_GPIO_PORT, NRF24L01P_CS_PIN );
}

void DisableSlave()
{
    GPIO_SetBits( NRF24L01P_CS_GPIO_PORT, NRF24L01P_CS_PIN );
}

void EnableChip()
{
    GPIO_SetBits( NRF24L01P_CE_GPIO_PORT, NRF24L01P_CE_PIN );
}

void DisableChip()
{
    GPIO_ResetBits( NRF24L01P_CE_GPIO_PORT, NRF24L01P_CE_PIN );
}

}

NRF24L01P::NRF24L01P(SPIInterface::SPIName inName)
    :  mSPI( inName )
{
    // enable CS, CE, IRQ pin clock
    NRF24L01P_CS_CLK_Enable();
    NRF24L01P_CE_CLK_Enable();
    NRF24L01P_IRQ_CLK_Enable();

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    // init pin
    GPIO_InitTypeDef theGPIOInit;

    mMode = ChipMode::UNKNOWN;
    // configure Slave Select
    theGPIOInit.GPIO_Pin = NRF24L01P_CS_PIN;
    theGPIOInit.GPIO_Mode = GPIO_Mode_OUT;
    theGPIOInit.GPIO_OType = GPIO_OType_PP;
    theGPIOInit.GPIO_Speed = GPIO_Speed_50MHz;
    theGPIOInit.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init( NRF24L01P_CS_GPIO_PORT, &theGPIOInit );
    // CS

    // CE
    theGPIOInit.GPIO_Pin = NRF24L01P_CE_PIN;
    theGPIOInit.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init( NRF24L01P_CE_GPIO_PORT, &theGPIOInit );

    DisableSlave();
    DisableChip();

    //NOTE must be pullUp all the time
    //IRQ
    theGPIOInit.GPIO_Pin = NRF24L01P_IRQ_PIN;
    theGPIOInit.GPIO_Mode = GPIO_Mode_IN;
    theGPIOInit.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init( NRF24L01P_IRQ_GPIO_PORT, &theGPIOInit );
    // init spi and default values

//    SYSCFG_EXTILineConfig(NRF24L01P_IRQ_PORT_SOURCE, NRF24L01P_IRQ_PIN_SOURCE);
//    EXTI_InitTypeDef   EXTI_InitStructure;
//    EXTI_InitStructure.EXTI_Line = NRF24L01P_IRQ_EXT_LINE;
//    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
//    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//    EXTI_Init(&EXTI_InitStructure);

//    /* Enable and set Button EXTI Interrupt to the lowest priority */
//    NVIC_InitTypeDef   NVIC_InitStructure;
//    NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (0x0F - 1);
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);

    Init();
}

NRF24L01P::~NRF24L01P()
{
}

void NRF24L01P::SetRfFrequency(uint16_t inFrequency )
{
    if ( ( inFrequency < NRF24L01P_MIN_RF_FREQUENCY ) || ( inFrequency > NRF24L01P_MAX_RF_FREQUENCY ) ) {
        assert_param( false );
        return;
    }
    uint8_t channel = ( inFrequency - NRF24L01P_MIN_RF_FREQUENCY ) & 0x7F;
    SetRegister(_NRF24L01P_REG_RF_CH, channel);
}

uint16_t NRF24L01P::GetRfFrequency()
{
    uint16_t channel = GetRegister(_NRF24L01P_REG_RF_CH) & 0x7F;
    return ( channel + NRF24L01P_MIN_RF_FREQUENCY );
}

void NRF24L01P::SetRfOutputPower( TX_PWR inPwr )
{
    uint8_t theRFSetup = GetRegister(_NRF24L01P_REG_RF_SETUP) & ~_NRF24L01P_RF_SETUP_RF_PWR_MASK;
    switch ( inPwr )
    {
    case TX_PWR::ZERO_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_0DBM;
        break;
    case TX_PWR::MINUS_6_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM;
        break;
    case TX_PWR::MINUS_12_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM;
        break;
    case TX_PWR::MINUS_18_DB:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM;
        break;
    default:
        assert_param( false );
        return;
    }
    SetRegister(_NRF24L01P_REG_RF_SETUP, theRFSetup);
}

NRF24L01P::TX_PWR NRF24L01P::GetRfOutputPower()
{
    uint8_t theRFSetup = GetRegister(_NRF24L01P_REG_RF_SETUP) & _NRF24L01P_RF_SETUP_RF_PWR_MASK;

    switch ( theRFSetup )
    {
    case _NRF24L01P_RF_SETUP_RF_PWR_0DBM:
        return TX_PWR::ZERO_DB;

    case _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM:
        return TX_PWR::MINUS_6_DB;

    case _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM:
        return TX_PWR::MINUS_12_DB;

    case _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM:
        return TX_PWR::MINUS_18_DB;

    default:
        assert_param( false );
        return TX_PWR::TX_PWR_INVALID;
    }
}

void NRF24L01P::SetAirDataRate( NRF24L01P::DataRate inDataRate )
{
    uint8_t theRFSetup = GetRegister(_NRF24L01P_REG_RF_SETUP) & ~_NRF24L01P_RF_SETUP_RF_DR_MASK;
    switch ( inDataRate )
    {
    case DataRate::DR_250_KBPS:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_DR_250KBPS;
        break;
    case DataRate::DR_1_MBPS:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_DR_1MBPS;
        break;
    case DataRate::DR_2_MBPS:
        theRFSetup |= _NRF24L01P_RF_SETUP_RF_DR_2MBPS;
        break;
    default:
        assert_param( false );
        return;
    }
    SetRegister(_NRF24L01P_REG_RF_SETUP, theRFSetup);
}

NRF24L01P::DataRate NRF24L01P::GetAirDataRate()
{
    uint8_t theRFSetup = GetRegister(_NRF24L01P_REG_RF_SETUP) & _NRF24L01P_RF_SETUP_RF_DR_MASK;
    switch ( theRFSetup )
    {
    case _NRF24L01P_RF_SETUP_RF_DR_250KBPS:
        return DataRate::DR_250_KBPS;

    case _NRF24L01P_RF_SETUP_RF_DR_1MBPS:
        return DataRate::DR_1_MBPS;

    case _NRF24L01P_RF_SETUP_RF_DR_2MBPS:
        return DataRate::DR_2_MBPS;

    default:
        assert_param( false );
        return DataRate::DR_INVALID;
    }
}

void NRF24L01P::SetCrcWidth( NRF24L01P::CRCWidth inCRCWidth )
{
    uint8_t config = GetRegister(_NRF24L01P_REG_CONFIG) & ~_NRF24L01P_CONFIG_CRC_MASK;

    switch ( inCRCWidth )
    {
    case CRCWidth::CRC_NONE:
        config |= _NRF24L01P_CONFIG_CRC_NONE;
        break;
    case CRCWidth::CRC_8_BIT:
        config |= _NRF24L01P_CONFIG_CRC_8BIT;
        break;
    case CRCWidth::CRC_16_BIT:
        config |= _NRF24L01P_CONFIG_CRC_16BIT;
        break;
    default:
        assert_param( false );
        return;
    }
    SetRegister(_NRF24L01P_REG_CONFIG, config);
}

NRF24L01P::CRCWidth NRF24L01P::GetCrcWidth()
{
    uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG) & _NRF24L01P_CONFIG_CRC_MASK;

    switch ( theConfig )
    {
    case _NRF24L01P_CONFIG_CRC_NONE:
        return CRCWidth::CRC_NONE;
    case _NRF24L01P_CONFIG_CRC_8BIT:
        return CRCWidth::CRC_8_BIT;
    case _NRF24L01P_CONFIG_CRC_16BIT:
        return CRCWidth::CRC_16_BIT;
    default:
        assert_param( false );
        return CRCWidth::CRC_INVALID;
    }
}

void NRF24L01P::SetRxAddress( unsigned long long address, RX_PIPE inPipe )
{
    uint8_t theWidth = 5;
    if ( inPipe > RX_PIPE::PIPE_1 )
    {
        theWidth = 1;
    }

    uint8_t theRxAddrPxRegister = _NRF24L01P_REG_RX_ADDR_P0 + static_cast<uint8_t>( inPipe );
    uint8_t cn = (_NRF24L01P_SPI_CMD_WR_REG | (theRxAddrPxRegister & _NRF24L01P_REG_ADDRESS_MASK));
    EnableSlave();
    mSPI.WriteRead(cn);
    for ( int i =0; i < theWidth; i++ )
    {
        mSPI.WriteRead((uint8_t) (address & 0xFF));
        address >>= 8;
    }
    DisableSlave();
    uint8_t theEnRxAddr = GetRegister(_NRF24L01P_REG_EN_RXADDR);
    theEnRxAddr |= (1 << (static_cast<uint8_t>( inPipe )) );
    SetRegister(_NRF24L01P_REG_EN_RXADDR, theEnRxAddr);
}

void NRF24L01P::SetRxAddress(unsigned long msb_address,
                             unsigned long lsb_address,
                             NRF24L01P::RX_PIPE inPipe)
{
    unsigned long long address = ( ( (unsigned long long) msb_address ) << 32 ) | ( ( (unsigned long long) lsb_address ) << 0 );
    SetRxAddress( address, inPipe );
}

void NRF24L01P::SetTxAddress(unsigned long long address)
{
    uint8_t cn = (_NRF24L01P_SPI_CMD_WR_REG | (_NRF24L01P_REG_TX_ADDR & _NRF24L01P_REG_ADDRESS_MASK));
    EnableSlave();
    mSPI.WriteRead(cn);
    for ( int i =0; i < 5; i++ )
    {
        mSPI.WriteRead((uint8_t) (address & 0xFF));
        address >>= 8;
    }
    DisableSlave();
}

void NRF24L01P::SetTxAddress(unsigned long msb_address, unsigned long lsb_address)
{
    unsigned long long address = ( ( (unsigned long long) msb_address ) << 32 ) | ( ( (unsigned long long) lsb_address ) << 0 );

    SetTxAddress(address);
}

unsigned long long NRF24L01P::GetRxAddress( NRF24L01P::RX_PIPE inPipe )
{
    uint8_t theWidth = 5;
    if ( RX_PIPE::PIPE_1 < inPipe )
    {
        theWidth = 1;
    }

    uint8_t theRxAddrPxRegister = _NRF24L01P_REG_RX_ADDR_P0 + static_cast<uint8_t>(inPipe);
    uint8_t cn = (_NRF24L01P_SPI_CMD_RD_REG | (theRxAddrPxRegister & _NRF24L01P_REG_ADDRESS_MASK));

    unsigned long long address = 0;
    EnableSlave();
    mSPI.WriteRead(cn);
    for ( int i = 0; i< theWidth; i++ ) {
        address |= ( ( (unsigned long long)( mSPI.WriteRead(_NRF24L01P_SPI_CMD_NOP) & 0xFF ) ) << (i*8) );
    }
    DisableSlave();
    if ( RX_PIPE::PIPE_1 < inPipe )
    {
        address |= ( GetRxAddress( RX_PIPE::PIPE_1 ) & ~((unsigned long long) 0xFF) );
    }

    return address;
}

unsigned long long NRF24L01P::GetTxAddress()
{
    uint8_t cn = (_NRF24L01P_SPI_CMD_RD_REG | (_NRF24L01P_REG_TX_ADDR & _NRF24L01P_REG_ADDRESS_MASK));
    unsigned long long address = 0;
    EnableSlave();
    mSPI.WriteRead(cn);
    for ( int i = 0; i < 5; i++ )
    {
        address |= ( (unsigned long long)( mSPI.WriteRead(_NRF24L01P_SPI_CMD_NOP) & 0xFF ) << (i*8) );
    }
    DisableSlave();
    return address;
}

void NRF24L01P::SetTransferSize( uint8_t inSize, NRF24L01P::RX_PIPE inPipe)
{
    if ( ( inSize < 0 ) || ( inSize > _NRF24L01P_RX_FIFO_SIZE ) )
    {
        assert_param( false );
        return;
    }

    uint8_t theRxPwPxRegister = _NRF24L01P_REG_RX_PW_P0 + static_cast<uint8_t>( inPipe );
    SetRegister(theRxPwPxRegister, ( inSize & _NRF24L01P_RX_PW_Px_MASK ) );
}

int NRF24L01P::GetTransferSize(NRF24L01P::RX_PIPE inPipe)
{
    uint8_t theRxPwPxRegister = _NRF24L01P_REG_RX_PW_P0 + static_cast<uint8_t>( inPipe );
    uint8_t size = GetRegister(theRxPwPxRegister);
    return ( size & _NRF24L01P_RX_PW_Px_MASK );
}

void NRF24L01P::SetReceiveMode()
{
    if ( ChipMode::POWER_DOWN == mMode ) PowerUp();
    uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG);
    theConfig |= _NRF24L01P_CONFIG_PRIM_RX;
    SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
    mMode = ChipMode::RX;
}

void NRF24L01P::SetTransmitMode()
{
    if ( ChipMode::POWER_DOWN == mMode ) PowerUp();
    uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG);
    theConfig &= ~_NRF24L01P_CONFIG_PRIM_RX;
    SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
    mMode = ChipMode::TX;
}

void NRF24L01P::PowerUp()
{
    uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG);
    theConfig |= _NRF24L01P_CONFIG_PWR_UP;
    SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
    // Wait until the nRF24L01+ powers up
    delayMicroseconds( _NRF24L01P_TIMING_Tpd2stby_us );
    mMode = ChipMode::STANDBY;
}

void NRF24L01P::PowerDown()
{
    uint8_t theConfig = GetRegister(_NRF24L01P_REG_CONFIG);
    theConfig &= ~_NRF24L01P_CONFIG_PWR_UP;
    SetRegister(_NRF24L01P_REG_CONFIG, theConfig);
    // Wait until the nRF24L01+ powers up
    delayMicroseconds( _NRF24L01P_TIMING_Tpd2stby_us );
    mMode = ChipMode::POWER_DOWN;
}

void NRF24L01P::SetEnable(bool inEnable)
{
    mEnabled = inEnable;
    if ( mEnabled )
    {
        EnableChip();
        delayMicroseconds(_NRF24L01P_TIMING_Tpece2csn_us );
    }
    else
    {
        DisableChip();
    }
}

int NRF24L01P::write(RX_PIPE pipe, char *data, int count)
{
    DisableChip();
    if ( count <= 0 )
        return 0;

    if ( count > _NRF24L01P_TX_FIFO_SIZE )
        count = _NRF24L01P_TX_FIFO_SIZE;

    // Clear the Status bit
    SetRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_TX_DS);
    EnableSlave();
    mSPI.WriteRead(_NRF24L01P_SPI_CMD_WR_TX_PAYLOAD);
    for ( int i = 0; i < count; i++ )
        mSPI.WriteRead(*data++);
    DisableSlave();

    ChipMode originalMode = mMode;
    SetTransmitMode();

    EnableChip();
    delayMicroseconds(_NRF24L01P_TIMING_Thce_us);
    DisableChip();

    while ( !( GetStatusRegister() & _NRF24L01P_STATUS_TX_DS ) );

    // Clear the Status bit
    SetRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_TX_DS);
    if ( originalMode == ChipMode::RX )
        SetReceiveMode();

    SetEnable( mEnabled );
    return count;
}

int NRF24L01P::read( RX_PIPE pipe, uint8_t *data, int count)
{
    if ( count <= 0 ) return 0;
    if ( count > _NRF24L01P_RX_FIFO_SIZE ) count = _NRF24L01P_RX_FIFO_SIZE;
    if ( Readable(pipe) )
    {
        EnableSlave();
        mSPI.WriteRead(_NRF24L01P_SPI_CMD_R_RX_PL_WID);
        int rxPayloadWidth = mSPI.WriteRead(_NRF24L01P_SPI_CMD_NOP);
        DisableSlave();

        if ( ( rxPayloadWidth < 0 ) || ( rxPayloadWidth > _NRF24L01P_RX_FIFO_SIZE ) )
        {
            EnableSlave();
            mSPI.WriteRead(_NRF24L01P_SPI_CMD_FLUSH_RX);
            mSPI.WriteRead(_NRF24L01P_SPI_CMD_NOP);
            DisableSlave();

        }
        else
        {
            if ( rxPayloadWidth < count ) count = rxPayloadWidth;
            EnableSlave();
            mSPI.WriteRead(_NRF24L01P_SPI_CMD_RD_RX_PAYLOAD);
            for ( int i = 0; i < count; i++ )
            {
                *data++ = mSPI.WriteRead(_NRF24L01P_SPI_CMD_NOP);
            }
            DisableSlave();
            // Clear the Status bit
            SetRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_RX_DR);
            return count;
        }
    }
    else
    {
        return 0;
    }
    return -1;
}

bool NRF24L01P::Readable( NRF24L01P::RX_PIPE inPipe )
{
    uint8_t theStatus = GetStatusRegister();
    theStatus =      ( ( theStatus & _NRF24L01P_STATUS_RX_DR )
                       && ( ( ( theStatus & _NRF24L01P_STATUS_RX_P_NO ) >> 1 ) == ( static_cast<uint8_t>( inPipe ) & 0x7 ) ) );
    return theStatus;
}

void NRF24L01P::DisableAllRxPipes()
{
    SetRegister(_NRF24L01P_REG_EN_RXADDR, _NRF24L01P_EN_RXADDR_NONE);
}

void NRF24L01P::DisableAutoAcknowledge()
{
    SetRegister(_NRF24L01P_REG_EN_AA, _NRF24L01P_EN_AA_NONE);
}

void NRF24L01P::enableAutoAcknowledge( NRF24L01P::RX_PIPE inPipe )
{
    if ( ( inPipe < RX_PIPE::PIPE_0 ) || ( inPipe > RX_PIPE::PIPE_5 ) )
    {
        assert_param( false);
    }

    uint8_t theEEAA = GetRegister(_NRF24L01P_REG_EN_AA);
    theEEAA |= ( 1 << ( static_cast<uint8_t> ( inPipe )) );
    SetRegister(_NRF24L01P_REG_EN_AA, theEEAA);
}

void NRF24L01P::DisableAutoRetransmit()
{
    SetRegister(_NRF24L01P_REG_SETUP_RETR, _NRF24L01P_SETUP_RETR_NONE);
}


void NRF24L01P::Init()
{
    mSPI.Init();
    delayMicroseconds(_NRF24L01P_TIMING_Tundef2pd_us);    // Wait for Power-on reset
//    SetRegister(_NRF24L01P_REG_CONFIG, 0); // Power Down
//    SetRegister(_NRF24L01P_REG_STATUS,
//                _NRF24L01P_STATUS_MAX_RT|
//                _NRF24L01P_STATUS_TX_DS|
//                _NRF24L01P_STATUS_RX_DR);   // Clear any pending interrupts
//    //
//    // Setup default configuration
//    //
//    DisableAllRxPipes();
//    SetRfFrequency();
//    SetRfOutputPower();
//    SetAirDataRate();
//    SetCrcWidth();
//    SetTxAddress();
//    SetRxAddress();
//    DisableAutoAcknowledge();
//    DisableAutoRetransmit();
//    SetTransferSize();
    mMode = ChipMode::POWER_DOWN;
}

void NRF24L01P::Deinit()
{
    mSPI.Deinit();
}

uint8_t NRF24L01P::GetRegister(uint8_t inReg )
{
    uint8_t cn = (_NRF24L01P_SPI_CMD_RD_REG | (inReg & _NRF24L01P_REG_ADDRESS_MASK));
    EnableSlave();
    mSPI.WriteRead(cn);
    uint8_t dn = mSPI.WriteRead(_NRF24L01P_SPI_CMD_NOP);
    DisableSlave();
    return dn;
}

void NRF24L01P::SetRegister( uint8_t inReg, uint8_t inRegData )
{
    DisableChip();
    volatile uint8_t cn = (_NRF24L01P_SPI_CMD_WR_REG | (inReg & _NRF24L01P_REG_ADDRESS_MASK));
    EnableSlave();
    mSPI.WriteRead(cn);
    mSPI.WriteRead(inRegData & 0xFF);
    DisableSlave();
    SetEnable( mEnabled );
    delayMicroseconds(_NRF24L01P_TIMING_Tpece2csn_us );
}

int NRF24L01P::GetStatusRegister()
{
    EnableSlave();
    uint8_t status = mSPI.WriteRead( _NRF24L01P_SPI_CMD_WR_REG | _NRF24L01P_REG_STATUS);
    DisableSlave();
    return status;
}

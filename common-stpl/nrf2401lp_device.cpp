#include "nrf2401lp_device.h"

#define _NRF24L01P_SPI_CMD_RD_REG            0x00
#define _NRF24L01P_SPI_CMD_WR_REG            0x20
#define _NRF24L01P_SPI_CMD_NOP               0xff
#define _NRF24L01P_TIMING_Tpece2csn_us       4

uint8_t NRF24L01PDevice::getRegister(uint8_t inReg )
{
    EnableNRF24L01P();
    mWriteRead->WriteRead(inReg);
    uint8_t dn = mWriteRead->WriteRead(_NRF24L01P_SPI_CMD_NOP);
    DisableNRF24L01P();
    return dn;
}

void NRF24L01PDevice::setRegister( uint8_t inReg, uint8_t inRegData )
{
    DisableTransferNRF24L01P();
    volatile uint8_t cn = (_NRF24L01P_SPI_CMD_WR_REG | inReg);
    EnableNRF24L01P();
    mWriteRead->WriteRead(cn);
    mWriteRead->WriteRead(inRegData);
    DisableNRF24L01P();
    NRF2401LPDelayUs(_NRF24L01P_TIMING_Tpece2csn_us );
}

void NRF24L01PDevice::setWriteReadInterface(WriteReadInterface* inInterface)
{
    mWriteRead = inInterface;
}

//int NRF24L01P::GetStatusRegister()
//{
//    EnableNRF24L01P();
//    uint8_t status = mWriteRead->WriteRead( _NRF24L01P_SPI_CMD_WR_REG | _NRF24L01P_REG_STATUS);
//    DisableNRF24L01P();
//    return status;
//}

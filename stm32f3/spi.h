#ifndef SPI_H
#define SPI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stm32f3xx.h>

#ifdef STM32F303xC

/* Definition for SPIx clock resources */
#define SPI2_CLK_ENABLE()                __SPI2_CLK_ENABLE();

#define SPI2_SCK_GPIO_CLK_ENABLE()       __GPIOB_CLK_ENABLE()
#define SPI2_MISO_GPIO_CLK_ENABLE()      __GPIOB_CLK_ENABLE()
#define SPI2_MOSI_GPIO_CLK_ENABLE()      __GPIOB_CLK_ENABLE()

#define SPI2_FORCE_RESET()               __SPI2_FORCE_RESET()
#define SPI2_RELEASE_RESET()             __SPI2_RELEASE_RESET()

// SPI2 Baud RATE

#define SPI2_BAUDRATE_PRESCALER          SPI_BAUDRATEPRESCALER_4
/* Definition for SPIx Pins */

#define SPI2_SCK_PIN                     GPIO_PIN_13
#define SPI2_SCK_GPIO_PORT               GPIOB
#define SPI2_SCK_AF                      GPIO_AF5_SPI2

#define SPI2_MISO_PIN                    GPIO_PIN_14
#define SPI2_MISO_GPIO_PORT              GPIOB
#define SPI2_MISO_AF                     GPIO_AF5_SPI2

#define SPI2_MOSI_PIN                    GPIO_PIN_15
#define SPI2_MOSI_GPIO_PORT              GPIOB
#define SPI2_MOSI_AF                     GPIO_AF5_SPI2

#endif

bool InitSPI2();
bool SPI2WriteRead( uint8_t* inByte, uint8_t* outByte);
uint8_t getRegister( uint8_t inReg );

#ifdef __cplusplus
}
#endif

#endif // SPI_H

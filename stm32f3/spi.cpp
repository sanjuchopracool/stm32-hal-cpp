#include "spi.h"
#include "main.h"
#include  <stm32f3xx_hal_spi.h>

SPI_HandleTypeDef theSpi2Handle;

bool InitSPI2()
{
    theSpi2Handle.Instance               = SPI2;

    SPI_InitTypeDef& theInit            = theSpi2Handle.Init;
    theInit.BaudRatePrescaler           = SPI_BAUDRATEPRESCALER_16;
    theInit.Direction                   = SPI_DIRECTION_2LINES;
    theInit.CLKPhase                    = SPI_PHASE_1EDGE;
    theInit.CLKPolarity                 = SPI_POLARITY_LOW;
    theInit.CRCCalculation              = SPI_CRCCALCULATION_DISABLE;
    theInit.CRCPolynomial               = 7;
    theInit.CRCLength                   = SPI_CRC_LENGTH_8BIT;
    theInit.DataSize                    = SPI_DATASIZE_8BIT;
    theInit.FirstBit                    = SPI_FIRSTBIT_MSB;
    theInit.NSS                         = SPI_NSS_SOFT;
    theInit.TIMode                      = SPI_TIMODE_DISABLE;
    theInit.NSSPMode                    = SPI_NSS_PULSE_DISABLE;
    theInit.Mode                        = SPI_MODE_MASTER;

    return ( HAL_SPI_Init( &theSpi2Handle ) == HAL_OK );
}


bool SPI2WriteRead( uint8_t* inByte, uint8_t* outByte )
{
    bool theStatus = (HAL_SPI_TransmitReceive(&theSpi2Handle, inByte, outByte, 1, 100) == HAL_OK);
    return theStatus;
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    GPIO_InitTypeDef  GPIO_InitStruct;

    if(hspi->Instance == SPI2)
    {
        /*##-1- Enable peripherals and GPIO Clocks #################################*/
        /* Enable GPIO TX/RX clock */
        SPI2_SCK_GPIO_CLK_ENABLE();
        SPI2_MISO_GPIO_CLK_ENABLE();
        SPI2_MOSI_GPIO_CLK_ENABLE();
        /* Enable SPI clock */
        SPI2_CLK_ENABLE();

        /*##-2- Configure peripheral GPIO ##########################################*/
        /* SPI SCK GPIO pin configuration  */
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
        GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;

        GPIO_InitStruct.Pin       = SPI2_SCK_PIN;
        GPIO_InitStruct.Alternate = SPI2_SCK_AF;
        HAL_GPIO_Init(SPI2_SCK_GPIO_PORT, &GPIO_InitStruct);

        /* SPI MISO GPIO pin configuration  */
        GPIO_InitStruct.Pin       = SPI2_MISO_PIN;
        GPIO_InitStruct.Alternate = SPI2_MISO_AF;
        HAL_GPIO_Init(SPI2_MISO_GPIO_PORT, &GPIO_InitStruct);

        /* SPI MOSI GPIO pin configuration  */
        GPIO_InitStruct.Pin       = SPI2_MOSI_PIN;
        GPIO_InitStruct.Alternate = SPI2_MOSI_AF;
        HAL_GPIO_Init(SPI2_MOSI_GPIO_PORT, &GPIO_InitStruct);
    }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi)
{
    if(hspi->Instance == SPI2)
    {
        /*##-1- Reset peripherals ##################################################*/
        SPI2_FORCE_RESET();
        SPI2_RELEASE_RESET();

        /*##-2- Disable peripherals and GPIO Clocks ################################*/
        /* Configure SPI SCK as alternate function  */
        HAL_GPIO_DeInit(SPI2_SCK_GPIO_PORT, SPI2_SCK_PIN);
        /* Configure SPI MISO as alternate function  */
        HAL_GPIO_DeInit(SPI2_MISO_GPIO_PORT, SPI2_MISO_PIN);
        /* Configure SPI MOSI as alternate function  */
        HAL_GPIO_DeInit(SPI2_MOSI_GPIO_PORT, SPI2_MOSI_PIN);
    }
}

